/*
 * @Author: hucheng
 * @Date: 2020-05-10 12:32:08
 * @Description: here is des
 */
/* eslint-disable no-new */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
